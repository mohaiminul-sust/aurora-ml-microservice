import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.linear_model import LinearRegression, Ridge, Lasso
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.ensemble import RandomForestRegressor
import joblib

from convenience import dump_excel, dump_csv, model_dir

def prep_data(user_data, mode):
    columns_to_drop = ['index', 'guid', 'weighted_score', 'risk_score', 'basic', 'moderate', 'freedom']
    columns_to_drop.remove(mode)

    user_data = user_data.drop(columns_to_drop, axis=1)
    x = user_data.drop([mode], axis=1)
    y = user_data[mode]

    return x, y


def test_model(model, x_test, y_test):
    predicted_scores = model.predict(x_test)

    ## For Getting abs value in case of negetive vals
    predicted_scores = pd.Series(predicted_scores)
    # print(predicted_scores.abs())
    predicted_scores = predicted_scores.abs()
    predicted_scores = predicted_scores.to_numpy()
    # print(predicted_scores)

    mean_squared_error(y_test, predicted_scores)
    mae = mean_absolute_error(y_test, predicted_scores)
    # print('Mean Absolute Error:', round(mae, 5), 'degrees.')

    mse = mean_squared_error(y_test, predicted_scores)
    # print('Mean Squared Error:', round(mse, 5))

    r2 = r2_score(y_test, predicted_scores)
    # print('r2 Score:', round(r2, 5))

    # Calculate mean absolute percentage error (MAPE)
    mape = 100 * (mae / y_test)

    # Calculate and display accuracy
    accuracy = 100 - np.mean(mape)
    # print('Accuracy:', round(accuracy, 2), '%')

    return {
        "mean_absolute_error": round(mae, 5),
        "mean_squared_error": round(mse, 5),
        "r2_score": round(r2, 5),
        "accuracy": round(accuracy, 2)
    }


def matrices_serializer(model_name, res, model, x_test, y_test):
    res[model_name] = {
        "score": round(model.score(x_test, y_test), 3),
        "metrices": test_model(model, x_test, y_test)
    }
    return res


def feature_transformer():
    numeric_features = ['age', 'days_off', 'employment_duration', 'annual_salary', 'duration_of_policy']

    numeric_transformer = Pipeline(steps=[
        ('imputer', SimpleImputer(strategy='median')),
        ('scaler', StandardScaler())
    ])

    categorical_features = ['smoking_habit', 'industry_status', 'employer_tier', 'employment_nature', 'travel_requirement', 'work_hazard', 'institute', 'degree', 'field_of_study', 'work_distance']

    categorical_transformer = Pipeline(steps=[
        ('imputer', SimpleImputer(strategy='constant', fill_value='missing')),
        ('onehot', OneHotEncoder(handle_unknown='ignore'))
    ])

    # column transformer
    return ColumnTransformer(
        transformers=[
            ('num', numeric_transformer, numeric_features),
            ('cat', categorical_transformer, categorical_features)
        ]
    )

def relevent_features(user_data, mode):
    columns_to_drop = ['index', 'guid', 'weighted_score', 'basic', 'moderate', 'freedom', 'risk_score']
    columns_to_drop.remove(mode)
    user_data = user_data.drop(columns_to_drop, axis=1)

    trans_data = pd.get_dummies(user_data)
    # return trans_data.to_json(orient='records')

    data_corr = trans_data.corr()
    corr_target = abs(data_corr[mode])
    relevant_features = corr_target[corr_target>.1]

    return relevant_features.to_json()

def train_aurora(user_data, mode):
    x, y = prep_data(user_data, mode)
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=42)

    response = {}
    response['train_count'] = len(x_train)
    response['test_count'] = len(x_test)

    # feature transform and impute
    preprocessor = feature_transformer()

    # process pipeline with Linear Regressor
    # clf = Pipeline(steps=[
    #     ('preprocessor', preprocessor),
    #     ('classifier', LinearRegression())
    # ])
    # clf.fit(x_train, y_train)
    # response = matrices_serializer('linear_regressor', response, clf, x_test, y_test)
    # joblib.dump(clf, model_dir+'linear_pipeline_'+mode+'.sav')

    # # process pipeline with Ridge Regressor
    # rd = Pipeline(steps=[
    #     ('preprocessor', preprocessor),
    #     ('classifier', Ridge(alpha=0.001))
    # ])
    # rd.fit(x_train, y_train)
    # response = matrices_serializer('ridge_regressor', response, rd, x_test, y_test)
    # joblib.dump(rd, model_dir+'ridge_pipeline_'+mode+'.sav')

    # # process pipeline with Lasso Regressor
    # las = Pipeline(steps=[
    #     ('preprocessor', preprocessor),
    #     ('classifier', Lasso(alpha=0.0001, max_iter=10e5))
    # ])
    # las.fit(x_train, y_train)
    # response = matrices_serializer('lasso_regressor', response, las, x_test, y_test)
    # joblib.dump(las, model_dir+'lasso_pipeline_'+mode+'.sav')

    # process pipeline with Random Forest Regressor
    rf = Pipeline(steps=[
        ('preprocessor', preprocessor),
        ('classifier', RandomForestRegressor(n_estimators=2000, random_state=42))
    ])
    rf.fit(x_train, y_train)
    response = matrices_serializer('forest_regressor', response, rf, x_test, y_test)
    joblib.dump(rf, model_dir+'random_forest_pipeline_'+mode+'.sav')

    return response

def predict_aurora(user_data, mode):
    model_path = model_dir+'random_forest_pipeline_'+mode+'.sav'
    model = joblib.load(model_path)
    score = model.predict(user_data)
    return score[0]
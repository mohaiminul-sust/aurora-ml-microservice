import os
import json
import pandas as pd

from flask import Flask, request

from convenience import dump_csv, dump_excel, data_dir, formatted_data, model_exists, duplicate_by_guid
from predict import train_aurora, predict_aurora, relevent_features

app = Flask(__name__)

PATH_TO_DATA = data_dir + 'export.csv'

@app.route('/')
def index():
    return 'Aurora ML Service Module!'

@app.route('/data/count')
def count_dataset():
    csv_data = pd.read_csv(PATH_TO_DATA)
    return str(len(csv_data))

@app.route('/data/dropcolumn')
def drop_csv_column():
    csv_data = pd.read_csv(PATH_TO_DATA)
    col_to_drop = request.args.get('column_name')
    if col_to_drop == None:
        return {
            "error": "Column not specified!"
        }
    # column exists
    csv_data = csv_data.drop(col_to_drop, axis=1)
    dump_csv(csv_data, 'export')
    return csv_data.to_json(orient='records')

@app.route('/data/features')
def list_dataset():
    mode = request.args.get('mode')
    if mode == None:
        mode = 'risk_score'
    csv_data = pd.read_csv(PATH_TO_DATA, dtype={
        "smoking_habit": "category",
        "industry_status": "category",
        "employer_tier": "category",
        "employment_nature": "category",
        "travel_requirement": "category",
        "work_hazard": "category",
        "institute": "category",
        "degree": "category",
        "field_of_study": "category",
        "work_distance": "category"
    })
    if mode in ('risk_score', 'basic', 'moderate', 'freedom'):
        features = relevent_features(csv_data, mode)
        return features
    else:
        return {
            "error": "Mode NOT Listed!"
        }


@app.route('/data/train/')
def train_dataset():
    mode = request.args.get('mode')
    if mode == None:
        mode = 'risk_score'

    csv_data = pd.read_csv(PATH_TO_DATA, dtype={
        "smoking_habit": "category",
        "industry_status": "category",
        "employer_tier": "category",
        "employment_nature": "category",
        "travel_requirement": "category",
        "work_hazard": "category",
        "institute": "category",
        "degree": "category",
        "field_of_study": "category",
        "work_distance": "category"
    })

    if mode in ('risk_score', 'basic', 'moderate', 'freedom'):
        res = train_aurora(csv_data, mode)
        return res
    else:
        return {
            "error": "Mode NOT Listed!"
        }


@app.route('/data/predict/', methods=['POST'])
def predict_data():
    mode = request.args.get('mode')
    trainset = request.args.get('data')
    if mode == None:
        mode = 'risk_score'
    if not model_exists(mode):
        return {
            'error': 'Trained model doesn\'t exist'
        }
    if not mode in ('risk_score', 'basic', 'moderate', 'freedom'):
        return {
            "error": 'Mode parameter doesn\'t match!'
        }

    user_data = request.get_json()
    user_data = formatted_data(user_data)

    if user_data == None:
        return {
            "error": 'No user data found in POST!'
        }

    if trainset == 'trainset':
        csv_data = pd.read_csv(PATH_TO_DATA)
        # check for duplicates before adding to dataset
        if not duplicate_by_guid(user_data['guid'], csv_data):
            last_index = int(csv_data['index'].max())
            user_data['index'] = last_index + 1
            user_frame = pd.DataFrame([user_data])
            with open(PATH_TO_DATA, 'a') as f:
                user_frame.to_csv(f, header=False, index=False)

    # prepare data and predict
    user_data.pop('index')
    user_data.pop('guid')
    user_data = pd.DataFrame.from_dict(user_data, orient='index')
    # return user_data[0].to_json()
    predicted_score = predict_aurora(user_data.transpose(), mode)

    return {
        "risk_score": round(predicted_score, 2)
    }
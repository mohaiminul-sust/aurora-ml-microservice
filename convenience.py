import pandas as pd
import os
from collections import OrderedDict

# input & output directories
data_dir = './data/'
model_dir = './models/'


def dump_excel(data_frame, file_name):
    full_file_name = file_name + '.xls'
    print("\nDumping " + full_file_name + "...")
    data_frame.to_excel(data_dir + full_file_name)
    print("Dumping Finished!\n")


def dump_csv(data_frame, file_name):
    full_file_name = file_name + '.csv'
    print("\nExporting " + full_file_name + "...")
    data_frame.to_csv(data_dir + full_file_name, index=False)
    print("Exported!\n")


def formatted_data(user_data):
    # for formatting arbitrarily mixed data to sorted form
    sorted_keys = ['index', 'guid', 'age', 'smoking_habit', 'days_off', 'industry_status', 'employer_tier', 'employment_duration', 'annual_salary', 'employment_nature', 'travel_requirement', 'work_hazard', 'institute', 'degree', 'field_of_study', 'work_distance', 'duration_of_policy', 'risk_score', 'weighted_score', 'basic', 'moderate', 'freedom']

    reordered_dict = {k: user_data[k] for k in sorted_keys}
    return reordered_dict


def model_exists(mode):
    model_path = model_dir+'random_forest_pipeline_'+mode+'.sav'
    if os.path.exists(model_path):
        return True
    return False

def duplicate_by_guid(guid, df):
    matches = df.loc[df["guid"].isin([guid]),"index"].tolist()
    if len(matches) > 0:
        return True
    else:
        return False